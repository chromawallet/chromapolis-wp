Chromapolis white paper guide
=============================

## Editing markdown

A guide to markdown syntax can be found [here](https://www.markdownguide.org/basic-syntax#overview).

If you are uncomfortable with markdown, there are many good editors for markdown which offer live previewing and automatic formatting tools. For example [Typora](https://typora.io).

All editing should be done in *./src/chromapolis-wp-x.xx-SNAPSHOT.md* where x.xx is the most recent version.

To contribute your edits to the current snapshot, please make a [pull request](https://help.github.com/articles/about-pull-requests/).

## Versioning

Version numbering will be done according to the Maven convention. The current version is called 0.01-SNAPSHOT. When we believe it is ready we rename to 0.01, generate a PDF, and begin working on 0.02-SNAPSHOT.

Versioned markdown files are stored in *./src*. Release pdfs are stored in *./release*.

The document metadata also includes version information taken from git:

		title: Chromapolis Platform White Paper
		author: ChromaWay AB
		git_tag: v1.0
		git_commit: abcdeff
		git_date: 2014-08-17

##Other formats

We use [Pandoc](http://pandoc.org/) for converting between markdown and other formats. A guide to installation can be found [here](http://www.pandoc.org/installing.html). 

Example: to generate a standalone HTML file with a table of contents and custom CSS the command looks like this:

		pandoc -s --toc -c ./css/example.css ./src/example.md -o example.html

Typora uses pandoc for converting documents, pdfs can be exported using the GUI.

## Generating release documents

There is a process for generating release documents to ensure that we maintain consistency. Please do not generate pdfs ad-hoc for distribution. Instead use the latest pdf in *./release*.

## Styling and CSS

Stylesheets and fonts are found in *./css*. HTML templates are found in *./templates*.




